import os

NUMBER_OF_HISTORY = 15

NUMBER_OF_PID = 15

# Create pid_list to store the pid
pid_list = list()

# Create history_list to store the command history
history_list = list()


def main():
    while True:
        global pid_list
        global history_list
        # Print a prompt
        cmd_str = input("msh> ")
        # If the user types a blank line, your shell will, quietly and with no other output,
        # print another prompt and accept a new line of input
        if cmd_str == "" or cmd_str[0] == " ":
            continue

        # Check if the user input is quit or exit
        if cmd_str == "quit" or cmd_str == "exit":
            # The shell will exit with status zero if user input is quit or exit
            exit(0)

        # If user input is !n
        if cmd_str[0] == "!":
            # we will re-run the nth command and add the nth command to the history_list
            nth_command_in_history(cmd_str)
            continue

        # If user input is not !n
        if cmd_str[0] != "!":
            # we will add cmd_str to the history_list
            history_list = history_list + [cmd_str]

        # we will run whatever the user command is

        # String to list
        cmd_list = cmd_str.split()

        # Check if the user input is history
        if cmd_str == "history":
            # Print the last 15 command in the history_list
            command_is_history()
            continue

        # Check if the user input is showpids
        if cmd_str == "showpids":
            # Print the last 15 pid in the pid_list
            command_is_showpids()
            continue

        # Check if the user input is cd
        if cmd_list[0] == "cd":
            # Change the directory
            try:
                os.chdir(cmd_list[1])
            except FileNotFoundError:
                print("Directory not found. ")
            continue

        # run user command
        run_user_command(cmd_list)


def run_user_command(cmd_list):
    """
    run_user_command
    :param cmd_list: list of user command
    :return: nothing
    Does: create child process and call the execvp() in the child process
    """
    global pid_list
    # Create child process
    # We will call execvp() in the child process
    pid = os.fork()

    # If the pid is -1 the fork failed
    if pid == -1:
        print("fork failed. ")
        exit(0)
    # If the pid is 0 it will be the child process
    elif pid == 0:
        # Check if the command is supported
        try:
            os.execvp(cmd_list[0], cmd_list)
            exit(0)
        except FileNotFoundError:
            print(str(cmd_list[0]) + ":" + " Command not found")
            exit(0)
    # If the pid is > 0 it will be the parent process, we will add the pid to pid_list
    elif pid > 0:
        pid_list = pid_list + [str(pid)]

    # The parent process will wait for the child process to be terminated
    os.wait()


def command_is_showpids():
    """
    command_is_showpids
    :return: nothing
    :Does: if the user command is showpids, list the last 15 pid
    """
    global pid_list
    count_pid = 0

    # If there is less than 15 pid in the list,
    # print pid from index 0 to length of pid_list.
    # If there is more than 15 pid in the list,
    # print pid from starting_index to length of pid_list.
    starting_index = len(pid_list) - NUMBER_OF_PID
    for index in range(0 if len(pid_list) <= NUMBER_OF_PID else starting_index, len(pid_list)):
        print(str(count_pid + 1) + ": " + str(pid_list[index]))
        count_pid = count_pid + 1
        if count_pid >= NUMBER_OF_PID:
            break


def command_is_history():
    """
    command_is_history
    :return nothing
    :Does: if the user command is history, list the last 15 commands entered by the user
    """
    global history_list
    count_history = 0

    # If there is less than 15 elements in the list,
    # start printing from the index 0, and only print the commands the user has entered.
    # If there is more than 15 elements in the list start printing from the starting_index
    starting_index = len(history_list) - NUMBER_OF_HISTORY
    for index in range(0 if len(history_list) <= NUMBER_OF_HISTORY else starting_index,
                       len(history_list)):
        print(str(count_history + 1) + ": " + str(history_list[index]))
        count_history = count_history + 1
        if count_history >= NUMBER_OF_HISTORY:
            break


def nth_command_in_history(user_input_n_str):
    """
    nth_command_in_history
    :param user_input_n_str: user command string
    :return history_list: list the history of command
    :does: check if history_list is empty
           check if number n contains character
           check if number n is 1 or greater than 1
           re-run the nth command in the history_list and add the nth command to the history_list
    """
    global pid_list
    global history_list

    # Check if history_list is empty
    if history_list is None:
        print("Command not in history.")
        return

    # Check if n is number and check if n is >= 1

    # Check if n includes character
    # Check if n only includes number
    if not user_input_n_str[1:].isnumeric():
        print("Command not in history.")
        return

    # Check if the number n is >= 1
    # We checked if it is numeric,
    # so there will be no - symbol,
    # so we do not have to check if it is negative number,
    # but we have to check if n is 0
    if int(user_input_n_str[1:]) < 1:
        print("Command not in history.")
        return

    # We will not need ! and we only need number n
    user_input_n = int(user_input_n_str[1:])

    # Check if the n is out of range
    # If there are less than 15 elements in the list, number n cannot be greater than the length of list
    if len(history_list) <= NUMBER_OF_HISTORY and user_input_n > len(history_list):
        print("Command not in history.")
        return
    # If there are more than 15 elements in the list, it will print the last 15 elements
    # Let's say we have 20 elements in the list, we start looking up from 5th element
    # Let's say we have 100 elements in the list, we start looking up from the 75th element
    starting_index = len(history_list) - NUMBER_OF_HISTORY
    # If there are more than 15 elements in the list, the first history will be at starting_index,
    # and starting_index + user_input_n cannot be greater than the length of list
    if len(history_list) > NUMBER_OF_HISTORY and starting_index + user_input_n > len(history_list):
        print("Command not in history.")
        return

    # Look up the nth command and store the nth command is the history_list
    # If there are less than 15 elements in the list
    # the 1st history is the 0th element in the list
    # the nth history is the n - 1 th element in the list
    if len(history_list) <= NUMBER_OF_HISTORY:
        command_in_history = history_list[user_input_n - 1]
    # If there are more than 15 elements in the list
    # the 1st history is at the starting_index - 1 in the list
    # the nth history is at the starting_index + user_input_n - 1 in the list
    # to find the nth history, you look at the starting_index and move afterward user_input_n numbers
    if len(history_list) > NUMBER_OF_HISTORY:
        command_in_history = history_list[starting_index + user_input_n - 1]

    # Store command_in_history in history_list
    # Add string to the rear of the list
    history_list = history_list + [command_in_history]

    # String to list
    cmd_list = command_in_history.split()

    # Check if the nth command is history
    if command_in_history == "history":
        command_is_history()
        return

    # Check if the nth command is showpids
    if command_in_history == "showpids":
        command_is_showpids()
        return

    # Check if the nth command is cd
    if cmd_list[0] == "cd":
        # Change the directory
        try:
            os.chdir(cmd_list[1])
        except FileNotFoundError:
            print("Directory not found. ")
        return

        # run user command
    run_user_command(cmd_list)
    return


main()
