import os
import time


def main():
    print("+-------------------Welcome-------------------+")
    print("|                                             |")
    print("|          Welcome to myshell program         |")
    print("|                                             |")
    print("+---------------------------------------------+")

    while True:
        t = time.ctime()
        print("Time: " + str(t))
        print("-----------------------------------------------")

        cwd = os.getcwd()
        print("Current Directory: " + cwd)
        print("-----------------------------------------------")

        # Print directories

        print("Directories: ")

        # Store directory names to folder_list
        folder_list = [dI for dI in os.listdir('.') if os.path.isdir(os.path.join('.', dI))]
        # If the folder_list is empty, it will not print the directory name
        if len(folder_list) == 0:
            print("There is no subdirectory in the current directory. ")
        # If the folder_list is not empty, it will print the directory name
        folder_index = 0
        folder_quit = ""
        while folder_index < len(folder_list):
            if folder_quit == "q":
                break
            print("          " + str(folder_index) + " Directories: " + folder_list[folder_index])
            folder_index = folder_index + 1
            # You can only show 5 lines of file names in one page
            # You will need to implement Prev, and Next Operations so that the menu fits
            # on one screen
            if folder_index % 5 == 0:
                n_or_p = input("Hit N for Next or hit P for prev. "
                               "If you do not want to display additional directories, "
                               "press q to quit displaying directories ")
                folder_quit = n_or_p
                if folder_quit == "q":
                    continue
                while True:
                    if n_or_p == "n":
                        break
                    if n_or_p == "p":
                        # User should not press p in the first page
                        # print(str(folder_index) + " Directories: " + folder_list[folder_index]) is before
                        # folder_index = folder_index + 1
                        # So in the second page, the file_index is 10, but the last file it will print is at file_index 9
                        # So file_index - 10 is 0, in the next iteration, it will get started printing at file_index 0
                        # It will print the file in the first page
                        # It will apply to page 3, 4, 5, 6 ......
                        folder_index = folder_index - 10

                        # Of course a user may type “P” ten times in succession, so
                        # you shouldn’t try to move the “window” of files
                        # you are showing to “before” the first

                        # If the user press p in the first page
                        # it will tell the user that this is the first page and
                        # it will display files in the first page
                        # so if the file_index is < 0, we need to set it to 0
                        if folder_index < 0:
                            print("You reach the first page. There is no directories before the directory 0. "
                                  "So it would not display any page before the first page. ")
                            folder_index = 0
                        break

                    # If the user did not choose N or P,
                    # let the user know that they need to choose N or P
                    n_or_p = input("You should choose N or P. "
                                   "Hit N for Next or hit P for prev. "
                                   "If you do not want to display additional directories, "
                                   "press q to quit displaying directories ")
                    folder_quit = n_or_p
                    if folder_quit == "q":
                        break

        print("-----------------------------------------------")

        # Print files

        print("Files: ")

        # Store file names to files_list
        files_list = [f for f in os.listdir('.') if os.path.isfile(f)]
        # If the files_list is empty, it will not print the file name
        if len(files_list) == 0:
            print("There is no file in the current directory. ")
        # If the files_list is not empty, it will print the file name
        file_index = 0
        file_quit = ""
        while file_index < len(files_list):
            if file_quit == "q":
                break
            print("          " + str(file_index) + " File: " + files_list[file_index])
            file_index = file_index + 1
            # You can only show 5 lines of file names in one page
            # You will need to implement Prev, and Next Operations so that the menu fits
            # on one screen
            if file_index % 5 == 0:
                n_or_p = input("Hit N for Next or hit P for prev. "
                               "If you do not want to display additional files, "
                               "press q to quit displaying files ")
                file_quit = n_or_p
                if file_quit == "q":
                    continue
                while True:
                    if n_or_p == "n":
                        break
                    if n_or_p == "p":
                        # User should not press p in the first page
                        # print(str(file_index) + " File: " + files_list[file_index]) is before
                        # file_index = file_index + 1
                        # So in the second page, the file_index is 10, but the last file it will print is at file_index 9
                        # So file_index - 10 is 0, in the next iteration, it will get started printing at file_index 0
                        # It will print the file in the first page
                        # It will apply to page 3, 4, 5, 6 ......
                        file_index = file_index - 10

                        # Of course a user may type “P” ten times in succession, so
                        # you shouldn’t try to move the “window” of files
                        # you are showing to “before” the first

                        # If the user press p in the first page
                        # it will tell the user that this is the first page and
                        # it will display files in the first page
                        # so if the file_index is < 0, we need to set it to 0
                        if file_index < 0:
                            print("You reach the first page. There is no file before the file 0. "
                                  "So it would not display any page before the first page. ")
                            file_index = 0
                        break

                    # If the user did not choose N or P,
                    # let the user know that they need to choose N or P
                    n_or_p = input("You should choose N or P. "
                                   "Hit N for Next or hit P for prev. "
                                   "If you do not want to display additional files, "
                                   "press q to quit displaying files ")
                    file_quit = n_or_p
                    if file_quit == "q":
                        break

        print("-----------------------------------------------")

        # Your shell will read a single key "command"
        # entered by the user, Followed by a file or directory number
        choose = input("""Operation: D  Display
           E  Edit
           R  Run
           C  Change Directory
           S  Sort Directory listing
           M  Move to Directory
           R  Remove File
           Q  Quit
        """)
        # If the user press q, it will quit
        if choose == "q" or choose == "Q":
            print("+---------------------------Thank you----------------------------+")
            print("|                                                                |")
            print("|          Thank you so much for playing myshell program         |")
            print("|                                                                |")
            print("+----------------------------------------------------------------+")
            exit(0)
        # If the user press e, it will open a text editor with a file
        elif choose == "e" or choose == "E":
            # If there is no file in the current directory,
            # it will not open a text editor
            if len(files_list) == 0:
                print("There is no file in the current directory. So it will not edit file. ")
                continue
            # If there is file in the current directory,
            # it will let the user choose which file to edit
            edit = input("Edit what? Choose a file number. ")
            while True:
                # Make sure the input is a number and less than the total number of the files
                if edit.isnumeric() and int(edit) < len(files_list):
                    break
                edit = input("You should choose a file number that is less "
                             "than the total number of the files. Edit what? "
                             "Choose a file number. ")
            # Convert string to integer
            edit = int(edit)
            os.system("nano " + str(files_list[edit]))
        # If the user press r, it will run an executable program
        elif choose == "r" or choose == "R":
            # If there is no file in the current directory,
            # it will not run an executable program
            if len(files_list) == 0:
                print("There is no file in the current directory. "
                      "So it will not edit file. ")
                continue
            # If there is file in the current directory,
            # it will let the user choose which file to run
            run = input("run what? Choose a file number. ")
            while True:
                # Make sure the input is a number and less than the total number of the files
                if run.isnumeric() and int(run) < len(files_list):
                    break
                run = input("You should choose a file number that is less "
                            "than the total number of the files. Edit what? "
                            "Choose a file number. ")
            # Convert string to integer
            run = int(run)
            os.system("./" + str(files_list[run]))
        # If the user press c, it will change working directory
        elif choose == "c" or choose == "C":
            # There is either a parent directory or subdirectory or both
            # So we do not need to check if the folder_list is empty or not
            change = input("Change to? Choose a directory number. ")
            while True:
                # Make sure the input is a number and less than the total number of the directories
                if change.isnumeric() and int(change) < len(folder_list):
                    break
                change = input("You should choose a directory number. "
                               "Edit what? Choose a directory number. ")
            # Convert string to integer
            change = int(change)
            try:
                os.chdir(str(folder_list[change]))
            except FileNotFoundError:
                print("Directory not found. ")


main()
